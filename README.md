# Faker-RDS

Like `Faker.js` but for relational databases. The key features are as follows:

- Completely (as in, 100%) deterministic sample generation with an
  easily-configurable seed state.
- Arbitrarily-long chains of foreign key reference columns.
- `JSON` column type support with arbitrary depth.
- modular export system allowing multiple export paths (csv is only current
  implementation though, PGSQL templates coming soon)
- Relatively fast
- Validation of configuration provided using [Pydantic](https://docs.pydantic.dev/)

## Installation

This library is built using [Poetry](https://python-poetry.org/), so installing
this library is as simple as adding it to your own Poetry dependencies list.

## Usage

Faker-RDS parses a `JSON` object using Pydantic which defines the data model
and all generator parameters involved. This object can be given as a file if
working locally but it can also be serialized and passed as a command-line
argument (read the CLI usage for details).

Once Faker-RDS parses the object it generates all the data in-memory and then
looks for user-defined export requests. It fulfills each export request
independently using the same data, so multiple export requests can be made.

## How it works

Under the hood, Faker-RDS is essentially 2 major components:

- A random data generator and
- A graph traversal algorithm specialized to handle relational databses

### Generators

Generators are impure functions that take a reference to a
`numpy.random.Generator` and generator options to produce a population of
observed samples of a certain random variable. The type definition of a
`TypeGenerator` is

```python
P = ParamSpec("P")
TypeGenerator = Callable[Concatenate[np.random.Generator, int, P], np.ndarray]
```

below is a list of currently supported generators.

| Type      | SQL Equivalent | Notable limitation(s)                      |
| --------- | -------------- | ------------------------------------------ |
| integer   | INTEGER        | N/A                                        |
| smallint  | SMALLINT       | N/A                                        |
| bigint    | BIGINT         | N/A                                        |
| float     | REAL           | N/A                                        |
| decimal   | DECIMAL        | N/A                                        |
| numeric   | NUMERIC        | N/A                                        |
| date      | DATE           | N/A                                        |
| time      | TIME           | N/A                                        |
| timestamp | TIMESTAMP      | N/A                                        |
| uuid      | UUID           | N/A                                        |
| json      | JSON           | No Foreign key columns to a JSON data type |

### The Graph Traversal

Relational data models can be thought of as a graph network, where the nodes of
the graph are individual columns of a table and the edges of the graph are
foreign key constraints (more on that later). In order to generate the data, we
need to traverse this graph, but because some nodes are statistically dependent
on other nodes, we cannot generate the data for every node in 1 single pass.

To efficiently perform graph traversal without revisiting already-generated
nodes, we push each node that couldn't be trivially generated onto a stack
during the first pass. On the second pass, we pop nodes off the stack. For each
node we pop off the stack, we follow all the edges it points to until we find
an already-generated node, at which point we back-fill each node we visited. We
update the list of generated nodes and move onto the next item in the stack.
Using this extra state allows us to only require 2 passes over the network to
fully complete node generation.

### Primary Key Constraints

With respect to the data model, Primary key constraints are practically
equivalent to combining the `UNIQUE` and `NOT NULL` constraints on a column(s).
For this reason you will not see a mention of primary key constraints in
Faker-RDS.

### Foreign Key Constraints

In SQL you can enforce that a column of a certain table must only contain
values which exist in another column. In other words, if column `A` has a
foreign key constraint imposed on it referencing column `B`, then
`A \subseteq B`. Furthermore, if column `A` has a unique constraint imposed on
it too then we also know that `|A| <= |B|`.

Now that we know the definition of a foreign key it should become obvious why
we cannot generate columns `A` and `B` in parallel: `A` is statistically
dependent on `B`! Thus in order to generate a population of a foreign key
column we need to first generate column `B` and then take a sample from column
`B` to produce column `A`. It is not guaranteed however that column `B` is not
also another foreign key column, thus we have to walk the list of dependencies
in its entirety before generating any foreign keys along the way.

### Unique Constraints

A unique constraint in SQL is quite self-explanatory, however we can categorize
these constraints into 2 forms: single-column unique constraints and
multi-column unique constraints. For example, the SQL constraint `UNIQUE(a,b,c)`
means that the combination of `a`, `b` and `c` must be unique (and hence its
cardinality is at least `|A|x|B|x|C|`) whereas the SQL constraint `UNIQUE(a)`
imposes that the column `A` only has 1 version of any value (excluding any
checks of NULL).

### NULL Constraints

## TODO

- [ ] Add support for the enum type using strings.
- [x] Add an email generator.
- [ ] Correctly handle self-referential tables
- [x] Fulfill the (single column) unique constraint.
- [x] fulfill the null / not null constraint.
- [x] Fix str representation of data when exporting `JSON` columns.
- [ ] Add support for the array monad on top of generators (JSON and foreign
      key columns will probably not be supported).
