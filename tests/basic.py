import sys

import numpy as np
from faker_rds.export import EXPORTS

from faker_rds.parser import GraphGenerator
from faker_rds.traverse import traverse


def main(argv: list[str]) -> int:
    if len(argv) != 2:
        print(
            "ERROR: Please provide exactly 1 argument for the file",
            file=sys.stderr,
        )
        sys.exit(-1)

    graph = GraphGenerator.parse_file(argv[1])
    rng = np.random.default_rng(seed=graph.seed)
    col_refs, columns = traverse(rng, graph)

    for export in graph.exports:
        EXPORTS[export.export](columns, col_refs, **export.params)

    return 0


sys.exit(main(sys.argv))
