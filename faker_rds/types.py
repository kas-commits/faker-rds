from __future__ import annotations
import weakref
from dataclasses import dataclass, field
from functools import reduce

from typing import Any, Sequence

import numpy as np


@dataclass(frozen=True)
class TCID:
    """
    A Table/Column Hashable pair of strings. Neither property is unique on
    its own but the combination of the two is garunteed unique in an RDS. You
    also can't cat the strings because you semantically need them separated.
    """

    table_name: str
    column_name: str


@dataclass(frozen=True)
class RefMetaData:
    samples: int
    unique: bool
    null_rate: float


@dataclass(frozen=True)
class FlatJsonMapping:
    """
    A single key-value combination, designed to be in a flat format. This means
    that all key-value pairs are stored adjacent to each other, regardless of
    the final json structure. The way to reconstruct the json structure is
    through the keys, which are not singular strings but instead a sequence of
    strings (stored as a tuple).
    """

    key: tuple[str, ...]
    value: list[Any]


@dataclass
class FlatJsonIterator:
    idx: int
    data_ptr: weakref.ReferenceType[FlatJson]

    def __next__(self) -> dict[str, Any] | None:
        data = self.data_ptr()
        if data is None:
            raise RuntimeError("Iterator was called on a deleted object")
        if self.idx == len(data):
            raise StopIteration()
        retval = data[self.idx]
        self.idx += 1
        return retval


@dataclass
class FlatJson:
    """
    The root object storing all the key-value pairs of a flattened Json column.
    """

    size: int
    num_nulls: int
    values: tuple[FlatJsonMapping, ...]
    positions: np.ndarray[Any, np.dtype[np.int64]]

    def __post_init__(self):
        print(
            f"{self.size=}, {self.num_nulls=}, {self.values=}, {self.positions=}"
        )
        input()

    def _make_row_for_idx(self, raw_idx: int) -> dict[str, Any] | None:

        # 1 indicates a null value, so return None
        if self.positions[raw_idx] == 1:
            return None

        num_nulls_to_shift = self.positions[:raw_idx].sum()
        idx = raw_idx - num_nulls_to_shift

        constructed_result: dict[str, Any] = dict()
        for entry in self.values:

            #  think of _it_dict as a pointer. We initialize the pointer at the
            #  root of the document, then iterate through the key sequence.
            # If we think of documents as a tree structure, then each iteration
            # directly translates to an increasae of the depth by 1.
            _it_dict = constructed_result

            for (_key_idx, _key_of_depth) in enumerate(entry.key):

                # if `_key_of_depth` is not found, then we need to add it. The
                # value could either be another document (i.e. a parent) or it
                # could be a non-document object (i.e. a leaf). We can deduce
                # which case it is based on the current depth we are in.
                #
                # NOTE: In Python, since `dict` is a mutable data structure,
                # modifying `_it_dict` will also modify `retval`, meaning that
                # `_it_dict` is a reference, not a copy. This wouldn't be true for
                # immutable data types like tuples, strings, etc...
                if _key_of_depth not in _it_dict:
                    _it_dict[_key_of_depth] = (
                        entry.value[idx]
                        if (_key_idx + 1) == len(entry.key)
                        else dict()
                    )

                # Now we know for sure the key is present because of the above
                _it_dict = _it_dict[_key_of_depth]

        return constructed_result

    def __len__(self):
        """
        Number of samples of the Json Data.
        NOTE: `len(self.values) != self.size`
        """
        return self.size

    __getitem__ = _make_row_for_idx

    def __iter__(self):
        return FlatJsonIterator(idx=0, data_ptr=weakref.ref(self))


@dataclass
class Column:
    tcid: TCID
    data: FlatJson | Sequence[Any]
    reference: TCID | None = field(default=None)
    # is_reference: bool = field(init=False)
    #
    # def __post_init__(self):
    #     self.is_reference = True if self.reference is not None else False

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx: int) -> dict[str, Any] | Any:
        return self.data[idx]


@dataclass
class Table:
    table_name: str
    columns: tuple[Column, ...]
    sample_size: int = field(init=False)

    @staticmethod
    def __assertion_key(a: int, b: int) -> int:
        assert a == b
        return a

    def __post_init__(self) -> None:
        self.sample_size = reduce(
            self.__assertion_key, map(lambda x: len(x.data), self.columns)
        )

    def __len__(self) -> int:
        return self.sample_size

    def __getitem__(self, idx: int) -> tuple[Any]:
        return tuple(col[idx] for col in self.columns)
