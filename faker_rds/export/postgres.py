from collections.abc import Iterable
from dataclasses import dataclass, field
from typing import Any, Callable, Collection
import enum
import psycopg
from strenum import StrEnum

from psycopg import sql as pgsql

from faker_rds.types import TCID, Column, Table
from faker_rds.util.sql import (
    group_tables_together,
    order_tables,
    normalize_for_sql,
)


COPY_BASE = pgsql.SQL("COPY {table_name}({column_names}) FROM STDIN")
INSERT_BASE = pgsql.SQL(
    "INSERT INTO {table_name}({column_names}) VALUES {entries}"
)


class Method(StrEnum):
    INSERT = enum.auto()
    COPY = enum.auto()


def join_sql_w_comma(seq: Iterable[pgsql.Composable]) -> pgsql.Composed:
    return pgsql.SQL(",").join(seq)


CursorType = psycopg.Cursor[tuple[Any, ...]]


@dataclass
class ExportContext:
    columns: Collection[Column]
    col_refs: dict[TCID, TCID]
    dbname: str
    host: str
    port: int
    user: str
    password: str
    conninfo: str = field(init=False)

    def __post_init__(self):
        self.conninfo = psycopg.conninfo.make_conninfo(
            dbname=self.dbname,
            host=self.host,
            port=self.port,
            user=self.user,
            password=self.password,
        )
        self.tables = order_tables(
            group_tables_together(self.columns), self.col_refs
        )

    def __call__(
        self,
        _export: Callable[[CursorType, Collection[Table]], None],
    ) -> None:
        with psycopg.connect(conninfo=self.conninfo) as conn:
            with conn.cursor() as cur:
                _export(cur, self.tables)
            conn.rollback()
            conn.close()


def _export_with_copy(cur: CursorType, tables: Collection[Table]) -> None:
    """
    Given a Cursor of an open connection and a collection of tables, export the
    data within the tables in the order they are presented using a `COPY`
    construct.
    """
    for table in tables:
        stmt_insert = COPY_BASE.format(
            table_name=pgsql.Identifier(table.table_name),
            column_names=join_sql_w_comma(
                map(
                    lambda col: pgsql.Identifier(col.tcid.column_name),
                    table.columns,
                )
            ),
        )
        print(stmt_insert.as_string(context=cur))
        with cur.copy(stmt_insert) as copy:
            for table_row in table:
                new_row = tuple(normalize_for_sql(x) for x in table_row)
                copy.write_row(new_row)


def _export_with_insert(cur: CursorType, tables: Collection[Table]) -> None:
    """
    Given a Cursor of an open connection and a collection of tables, export the
    data within the tables in the order they are presented using the `INSERT`
    construct.
    """
    for table in tables:
        stmt_insert = INSERT_BASE.format(
            table_name=pgsql.Identifier(table.table_name),
            column_names=join_sql_w_comma(
                map(
                    lambda col: pgsql.Identifier(col.tcid.column_name),
                    table.columns,
                )
            ),
            entries=join_sql_w_comma(
                (
                    pgsql.SQL("(")
                    + join_sql_w_comma(
                        (
                            pgsql.Literal(normalize_for_sql(col[table_idx]))
                            for col in table.columns
                        )
                    )
                    + pgsql.SQL(")")
                    for table_idx in range(len(table))
                ),
            ),
        )
        print(stmt_insert.as_string(context=cur))
        cur.execute(stmt_insert)


def export_to_postgres(
    columns: Collection[Column],
    col_refs: dict[TCID, TCID],
    dbname: str,
    host: str,
    port: int,
    user: str,
    password: str,
    method: Method = Method.COPY,
) -> None:
    export_context = ExportContext(
        columns=columns,
        col_refs=col_refs,
        dbname=dbname,
        host=host,
        port=port,
        user=user,
        password=password,
    )
    match method.upper():
        case Method.INSERT:
            export_context(_export_with_insert)
        case Method.COPY:
            export_context(_export_with_copy)
        case _:
            raise TypeError(f"{method=} not recognized")
