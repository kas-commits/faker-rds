from pathlib import Path
from typing import Collection

from faker_rds.types import TCID, Column
from faker_rds.util.pandas import export_to_pandas


def export_to_csv(
    columns: Collection[Column], col_refs: dict[TCID, TCID], path: Path | str
) -> None:
    del col_refs  # Not required.
    list_of_values = export_to_pandas(columns)

    if isinstance(path, Path):
        _path = path
    elif isinstance(path, str):
        _path = Path(path)
    else:
        raise TypeError(f"Expected Path or str, recieved {path=}")

    for (table_name, data) in list_of_values:
        data.to_csv(_path / (table_name + ".csv"), index=False)
