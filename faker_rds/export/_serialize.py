from typing import Callable, Collection, Concatenate, ParamSpec
import enum
from strenum import StrEnum

from faker_rds.export.postgres import export_to_postgres
from faker_rds.export.csv import export_to_csv
from faker_rds.types import Column, TCID


_P = ParamSpec("_P")
Exporter = Callable[
    Concatenate[Collection[Column], dict[TCID, TCID], _P], None
]


class SupportedExports(StrEnum):
    csv = enum.auto()
    CSV = enum.auto()

    postgres = enum.auto()
    POSTGRES = enum.auto()

    postgresql = enum.auto()
    POSTGRESQL = enum.auto()


EXPORTS: dict[str, Exporter] = {
    SupportedExports.csv: export_to_csv,
    SupportedExports.CSV: export_to_csv,
    SupportedExports.postgres: export_to_postgres,
    SupportedExports.POSTGRES: export_to_postgres,
    SupportedExports.postgresql: export_to_postgres,
    SupportedExports.POSTGRESQL: export_to_postgres,
}
