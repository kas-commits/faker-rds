from __future__ import annotations
from pydantic import BaseModel, Field
from typing import Any

from faker_rds.generators import SupportedGenerators
from faker_rds.export import SupportedExports


class ReferenceData(BaseModel):
    table: str
    column: str
    unique: bool = False
    null_rate: float = 0.0


class GeneratorData(BaseModel):
    type: SupportedGenerators
    unique: bool = False
    null_rate: float = 0.0
    params: dict[str, Any] = Field(default_factory=dict)


class JsonData(BaseModel):
    key: str
    value: list[JsonData] | GeneratorData


class ColumnGenerator(BaseModel):
    column_name: str
    unique: bool = False
    null_rate: float = 0.0
    data: ReferenceData | GeneratorData | list[JsonData]


class TableGenerator(BaseModel):
    table_name: str
    columns: list[ColumnGenerator]
    samples: int


class ExportData(BaseModel):
    export: SupportedExports
    params: dict[str, Any] = Field(default_factory=dict)


class GraphGenerator(BaseModel):
    seed: int = 0
    tables: list[TableGenerator]
    exports: list[ExportData] = Field(
        default_factory=lambda: [ExportData(export=SupportedExports.csv)]
    )
