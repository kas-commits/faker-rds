import datetime as dt

import numpy as np

from faker_rds.generators._util import inject_nulls
from faker_rds.generators.timestamp import DT_RANGE


def _convert_dt_to_date_str(x: np.datetime64) -> str:
    val: dt.datetime = x.item()
    if val.month >= 10:
        if val.day >= 10:
            return f"{val.year}-{val.month}-{val.day}"
        else:
            return f"{val.year}-{val.month}-0{val.day}"
    else:
        if val.day >= 10:
            return f"{val.year}-0{val.month}-{val.day}"
        else:
            return f"{val.year}-0{val.month}-0{val.day}"


@inject_nulls
def gen_dates(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    start_year: int = 1990,
    start_month: int = 1,
    start_day: int = 1,
    low_days: int = 0,
    high_days: int = DT_RANGE.days,
    serialize: bool = True,
) -> np.ndarray:

    assert low_days >= 0
    assert high_days >= low_days

    start_date = np.datetime64(
        dt.datetime(start_year, start_month, start_day).isoformat(sep=" ")
    )

    days = low_days + rng.choice(
        a=high_days - low_days, size=samples, replace=not unique
    )

    days_as_td = days * np.timedelta64(1, "D")

    values = start_date + days_as_td

    return (
        np.array(list(map(_convert_dt_to_date_str, values)))
        if serialize
        else values
    )
