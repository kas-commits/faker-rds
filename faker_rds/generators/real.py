import numpy as np

from faker_rds.generators._util import inject_nulls


@inject_nulls
def gen_floats(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    round: int | None = None,
    low: float = -1000.0,
    high: float = 1000.0,
) -> np.ndarray:
    del unique
    retval = rng.uniform(low=low, high=high, size=samples)
    return retval if round is None else retval.round(decimals=round)
