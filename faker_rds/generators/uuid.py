import uuid

import numpy as np

from faker_rds.generators._util import inject_nulls

MAX_INT_8 = (1 << 8) - 1
MAX_INT_16 = (1 << 16) - 1
MAX_INT_32 = (1 << 32) - 1
MAX_INT_48 = (1 << 48) - 1


@inject_nulls
def gen_uuids(
    rng: np.random.Generator, samples: int, unique: bool
) -> np.ndarray:
    """
    Generate UUIDS using the fields methadology. Use `help(uuid.UUID)` for more
    info, but in short:
    - x1: time_low (32-bit)
    - x2: time_mid (16-bit)
    - x3: time_high_version (16-bit)
    - x4: clock_seq_hi_version (8-bit)
    - x5: clock_seq_low (8-bit)
    - x6: node (48-bit)
    """
    x1 = rng.integers(low=0, high=MAX_INT_32, size=samples, dtype=np.uint32)
    x2 = rng.integers(low=0, high=MAX_INT_16, size=samples, dtype=np.uint16)
    x3 = rng.integers(low=0, high=MAX_INT_16, size=samples, dtype=np.uint16)
    x4 = rng.integers(low=0, high=MAX_INT_8, size=samples, dtype=np.uint8)
    x5 = rng.integers(low=0, high=MAX_INT_8, size=samples, dtype=np.uint8)
    # x6 = rng.integers(low=0, high=MAX_INT_48, size=samples, dtype=np.uint64)
    x6 = rng.choice(
        a=MAX_INT_48, size=samples, replace=not unique, shuffle=True
    )
    return np.array(
        list(
            map(
                lambda x: uuid.UUID(fields=tuple(map(lambda y: y.item(), x))),
                zip(x1, x2, x3, x4, x5, x6),
            )
        )
    )
