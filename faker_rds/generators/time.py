import datetime as dt

import numpy as np

from faker_rds.generators._util import inject_nulls


FIXED_DATE = np.datetime64("2020-01-01")


def _convert_dt_to_time_str(x: np.datetime64) -> str:
    val: dt.datetime = x.item()
    hr = ("" if val.hour >= 10 else "0") + str(val.hour)
    min = ("" if val.minute >= 10 else "0") + str(val.minute)
    sec = ("" if val.second >= 10 else "0") + str(val.second)
    return f"{hr}:{min}:{sec}"


@inject_nulls
def gen_times(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    low_hour: int = 0,
    high_hour: int = 24,
    low_min: int = 0,
    high_min: int = 60,
    low_sec: int = 0,
    high_sec: int = 60,
    serialize: bool = True,
) -> np.ndarray:
    assert low_hour >= 0
    assert high_hour <= 24
    assert low_hour < high_hour

    assert low_min >= 0
    assert high_min <= 60
    assert low_min < high_min

    assert low_sec >= 0
    assert high_sec <= 60
    assert low_sec < high_sec

    max_dt = (
        FIXED_DATE
        + np.timedelta64(high_hour, "h")
        + np.timedelta64(high_min, "m")
        + np.timedelta64(high_sec, "s")
    )

    min_dt = (
        FIXED_DATE
        + np.timedelta64(low_hour, "h")
        + np.timedelta64(low_min, "m")
        + np.timedelta64(low_sec, "s")
    )

    td_diff = int((max_dt - min_dt) / np.timedelta64(1, "s"))

    secs = rng.choice(a=td_diff, size=samples, replace=not unique)

    secs_as_td = secs * np.timedelta64(1, "s")

    values = min_dt + secs_as_td

    return (
        np.array(list(map(_convert_dt_to_time_str, values)))
        if serialize
        else values
    )
