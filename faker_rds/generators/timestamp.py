import datetime as dt

import numpy as np

from faker_rds.generators._util import inject_nulls

MIN_DATE = dt.datetime(1990, 1, 1)
MAX_DATE = dt.datetime(2025, 1, 1)
DT_RANGE = MAX_DATE - MIN_DATE


@inject_nulls
def gen_timestamps(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    start_year: int = MIN_DATE.year,
    start_month: int = MIN_DATE.month,
    start_day: int = MIN_DATE.day,
    low_days: int = 0,
    high_days: int = DT_RANGE.days,
    low_hour: int = 0,
    high_hour: int = 24,
    low_min: int = 0,
    high_min: int = 60,
    low_sec: int = 0,
    high_sec: int = 60,
    serialize: bool = True,
) -> np.ndarray:
    assert low_hour >= 0
    assert high_hour <= 24
    assert low_hour <= high_hour

    assert low_min >= 0
    assert high_min <= 60
    assert low_min <= high_min

    assert low_sec >= 0
    assert high_sec <= 60
    assert low_sec <= high_sec

    start_date = np.datetime64(
        dt.datetime(start_year, start_month, start_day).isoformat(sep=" ")
    )

    _max_end_date = (
        start_date
        + np.timedelta64(high_days, "D")
        + np.timedelta64(high_hour, "h")
        + np.timedelta64(high_min, "m")
        + np.timedelta64(high_sec, "s")
    )

    max_seconds = int((_max_end_date - start_date) / np.timedelta64(1, "s"))

    _min_end_date = (
        start_date
        + np.timedelta64(low_days, "D")
        + np.timedelta64(low_hour, "h")
        + np.timedelta64(low_min, "m")
        + np.timedelta64(low_sec, "s")
    )

    min_seconds = int((_min_end_date - start_date) / np.timedelta64(1, "s"))

    secs = min_seconds + rng.choice(
        a=max_seconds - min_seconds, size=samples, replace=not unique
    )

    secs_as_td = secs * np.timedelta64(1, "s")

    values = start_date + secs_as_td

    return np.vectorize(str)(values) if serialize else values
