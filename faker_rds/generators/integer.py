import numpy as np


from faker_rds.generators._util import inject_nulls


@inject_nulls
def gen_integers(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    low: int = -2147483648,
    high: int = 2147483647,
) -> np.ndarray:
    return low + rng.choice(
        a=high - low, size=samples, replace=not unique, shuffle=True
    )
