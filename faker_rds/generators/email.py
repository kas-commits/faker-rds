import numpy as np

from faker_rds.generators._util import inject_nulls

REPOSITORY_USER_NAME = [
    "Duchess",
    "Explore",
    "Craving",
    "Gyration",
    "Salvage",
    "Doodle",
    "Spearman",
    "False",
    "Tint",
    "Decompose",
    "Passerby",
    "Flatware",
    "Handcart",
    "Reconfirm",
    "Union",
    "Outer",
    "Untamed",
    "Politely",
    "Geranium",
    "Uneven",
    "Jawless",
]

REPOSITORY_DOMAIN = [
    "gmail.com",
    "tuta.io",
    "proton.me",
    "protonmail.com",
    "yahoo.com",
    "hotmail.com",
    "outlook.com",
]


@inject_nulls
def gen_emails(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    user_name: list[str] = REPOSITORY_USER_NAME,
    domain_names: list[str] = REPOSITORY_DOMAIN,
) -> np.ndarray:

    _user_names = rng.choice(a=user_name, size=samples)
    _name_randomizer = rng.choice(
        a=max(samples, 9999), size=samples, replace=not unique
    )
    _domain_names = rng.choice(a=domain_names, size=samples)

    return np.array(
        list(
            map(
                lambda x: f"{x[0]}{x[1]}@{x[2]}",
                zip(_user_names, _name_randomizer, _domain_names),
            )
        )
    )
