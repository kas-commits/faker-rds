"""
Serialized names of the default supported generators.
"""
import enum
from strenum import StrEnum


from faker_rds.generators._util import TypeGeneratorWithNulls

from faker_rds.generators.integer import gen_integers
from faker_rds.generators.date import gen_dates
from faker_rds.generators.timestamp import gen_timestamps
from faker_rds.generators.time import gen_times
from faker_rds.generators.real import gen_floats
from faker_rds.generators.uuid import gen_uuids
from faker_rds.generators.name import gen_names
from faker_rds.generators.email import gen_emails


class SupportedGenerators(StrEnum):
    integer = enum.auto()
    INTEGER = enum.auto()

    smallint = enum.auto()
    SMALLINT = enum.auto()

    bigint = enum.auto()
    BIGINT = enum.auto()

    decimal = enum.auto()
    DECIMAL = enum.auto()

    numeric = enum.auto()
    NUMERIC = enum.auto()

    real = enum.auto()
    REAL = enum.auto()

    double_precision = enum.auto()
    DOUBLE_PRECISION = enum.auto()

    float = enum.auto()
    FLOAT = enum.auto()

    date = enum.auto()
    DATE = enum.auto()

    time = enum.auto()
    TIME = enum.auto()

    timestamp = enum.auto()
    TIMESTAMP = enum.auto()

    uuid = enum.auto()
    UUID = enum.auto()

    real_name = enum.auto()
    REAL_NAME = enum.auto()

    email = enum.auto()
    EMAIL = enum.auto()


GENERATORS: dict[str, TypeGeneratorWithNulls] = {
    SupportedGenerators.integer: gen_integers,
    SupportedGenerators.INTEGER: gen_integers,
    SupportedGenerators.smallint: gen_integers,
    SupportedGenerators.SMALLINT: gen_integers,
    SupportedGenerators.bigint: gen_integers,
    SupportedGenerators.BIGINT: gen_integers,
    SupportedGenerators.float: gen_floats,
    SupportedGenerators.FLOAT: gen_floats,
    SupportedGenerators.real: gen_floats,
    SupportedGenerators.REAL: gen_floats,
    SupportedGenerators.double_precision: gen_floats,
    SupportedGenerators.DOUBLE_PRECISION: gen_floats,
    SupportedGenerators.decimal: gen_floats,
    SupportedGenerators.DECIMAL: gen_floats,
    SupportedGenerators.numeric: gen_floats,
    SupportedGenerators.NUMERIC: gen_floats,
    SupportedGenerators.date: gen_dates,
    SupportedGenerators.DATE: gen_dates,
    SupportedGenerators.time: gen_times,
    SupportedGenerators.TIME: gen_times,
    SupportedGenerators.timestamp: gen_timestamps,
    SupportedGenerators.TIMESTAMP: gen_timestamps,
    SupportedGenerators.uuid: gen_uuids,
    SupportedGenerators.UUID: gen_uuids,
    SupportedGenerators.real_name: gen_names,
    SupportedGenerators.REAL_NAME: gen_names,
    SupportedGenerators.email: gen_emails,
    SupportedGenerators.EMAIL: gen_emails,
}
