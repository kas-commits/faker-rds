import itertools as itools
import numpy as np

from faker_rds.generators._util import inject_nulls


REPO_FIRST_NAME = [
    "James",
    "Robert",
    "John",
    "Micheal",
    "David",
    "William",
    "Richard",
    "Joseph",
    "Thomas",
    "Charles",
    "Christopher",
    "Daniel",
    "Matthew",
    "Anthony",
    "Mark",
    "Donald",
    "Steven",
    "Paul",
    "Andrew",
    "Joshua",
    "Kenneth",
    "Kevin",
    "Brian",
    "George",
    "Timothy",
    "Ronald",
    "Edward",
    "Jason",
    "Jeffrey",
    "Ryan",
    "Jacob",
    "Gary",
    "Nicholas",
    "Eric",
    "Jonathan",
    "Stephen",
    "Larry",
    "Justin",
    "Scott",
    "Brandon",
    "Benjamin",
    "Samuel",
    "Gregory",
    "Alexander",
    "Frank",
    "Patrik",
    "Raymond",
    "Jack",
    "Dennis",
    "Jerry",
    "Tyler",
    "Aaron",
    "Jose",
    "Adam",
]

REPO_LAST_NAME = [
    "Smith",
    "Johnson",
    "Williams",
    "Brown",
    "Jones",
    "Garcia",
    "Miller",
    "Davis",
    "Rodriguez",
    "Martinez",
    "Hernandez",
    "Lopez",
    "Gonzales",
    "Wilson",
    "Anderson",
    "Thomas",
    "Taylor",
    "Moore",
    "Jackson",
    "Martin",
    "Lee",
    "Perez",
    "Thompson",
    "White",
    "Harris",
    "Sanchez",
    "Clark",
    "Ramirez",
    "Lewis",
    "Robinson",
    "Walker",
    "Young",
    "Allen",
    "King",
    "Wright",
    "Scott",
    "Torres",
    "Nguyen",
    "Hill",
    "Flores",
    "Green",
    "Adams",
    "Nelson",
    "Baker",
    "Hall",
    "Rivera",
    "Campbell",
    "Mitchell",
    "Carter",
    "Roberts",
    "Gomez",
    "Phillips",
    "Evans",
    "Diaz",
]


@inject_nulls
def gen_names(
    rng: np.random.Generator,
    samples: int,
    unique: bool,
    first_name: bool = True,
    last_name: bool = True,
) -> np.ndarray:
    assert first_name or last_name

    if first_name and not last_name:
        _first_names = rng.choice(
            a=REPO_FIRST_NAME, size=samples, replace=not unique
        )
        return _first_names
    elif not first_name and last_name:
        _last_names = rng.choice(
            a=REPO_LAST_NAME, size=samples, replace=not unique
        )
        return _last_names
    elif first_name and last_name:
        if unique:
            assert len(REPO_FIRST_NAME) * len(REPO_LAST_NAME) >= samples
        gen_pool = list(itools.product(REPO_FIRST_NAME, REPO_LAST_NAME))
        reduced_gen_pool = rng.choice(
            gen_pool, size=samples, replace=not unique
        )
        _retval = np.array(list(map(lambda x: " ".join(x), reduced_gen_pool)))
        return _retval
    else:
        raise Exception()
