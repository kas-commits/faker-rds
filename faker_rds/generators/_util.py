from typing import Any, Callable, Concatenate, ParamSpec

import numpy as np


def null_split(
    rng: np.random.Generator, samples: int, null_rate: float
) -> tuple[int, int]:
    nulls = rng.binomial(n=samples, p=null_rate)
    return (samples - nulls, nulls)


_P = ParamSpec("_P")
TypeGenerator = Callable[
    Concatenate[np.random.Generator, int, bool, _P], np.ndarray
]
TypeGeneratorWithNulls = Callable[
    Concatenate[np.random.Generator, int, bool, float, _P], list[Any]
]


def inject_nulls(_generator: TypeGenerator) -> TypeGeneratorWithNulls:
    def lifted_func(
        rng: np.random.Generator,
        samples: int,
        unique: bool,
        null_rate: float,
        *args,
        **kwargs,
    ) -> list[Any]:
        num_nulls = rng.binomial(n=samples, p=null_rate)
        num_non_nulls = samples - num_nulls
        non_null_data = _generator(rng, num_non_nulls, unique, *args, **kwargs)
        catted_vals = list(non_null_data) + [None for _ in range(num_nulls)]
        rng.shuffle(catted_vals)
        return catted_vals

    return lifted_func
