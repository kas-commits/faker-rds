from functools import reduce
from typing import Any, Collection

import numpy as np

from faker_rds.types import (
    TCID,
    FlatJson,
    FlatJsonMapping,
    Column,
    RefMetaData,
)
from faker_rds.parser import (
    GraphGenerator,
    ReferenceData,
    GeneratorData,
    JsonData,
)
from faker_rds.generators import GENERATORS


def _inner_traverse_json(
    rng: np.random.Generator,
    samples: int,
    _key: tuple[str, ...],
    _data: JsonData,
) -> tuple[FlatJsonMapping, ...]:

    _new_key = _key + (_data.key,)

    if isinstance(_data.value, list):

        for _inner in _data.value:
            if not isinstance(_inner, JsonData):
                raise TypeError(
                    f"Expected JsonResolved object, recieved {type(_inner)}"
                )

        return reduce(
            lambda x, y: x + y,
            map(
                lambda _x: _inner_traverse_json(rng, samples, _new_key, _x),
                _data.value,
            ),
        )

    elif isinstance(_data.value, GeneratorData):

        return (
            FlatJsonMapping(
                key=_new_key,
                value=GENERATORS[_data.value.type](
                    rng,
                    samples,
                    False,
                    0.0,
                    **_data.value.params,
                ),
            ),
        )

    else:
        raise TypeError(
            f"Expected list or list object, recieved {type(_data.value)}"
        )


def traverse_json(
    rng: np.random.Generator,
    ref_meta_data: RefMetaData,
    data: list[JsonData],
) -> FlatJson:
    """
    Helper function to traverse the parsed declration of a `JSON` column and
    produce the samples along the way.
    """
    positions = rng.binomial(
        n=1, p=ref_meta_data.null_rate, size=ref_meta_data.samples
    )
    num_nulls = positions.sum()
    return FlatJson(
        size=ref_meta_data.samples,
        num_nulls=num_nulls,
        values=reduce(
            lambda x, y: x + y,
            map(
                lambda _x: _inner_traverse_json(
                    rng, ref_meta_data.samples, tuple(), _x
                ),
                data,
            ),
        ),
        positions=positions,
    )


def subset_samples(
    rng: np.random.Generator, samples: Column, ref_meta_data: RefMetaData
) -> list[Any]:
    """
    Take a subset of the samples. If `unique` and `nullable` are set to `true`
    then we must have `size` <= len(samples), otherwise program will crash.
    """
    if (
        ref_meta_data.unique
        and (ref_meta_data.null_rate == 0.0)
        and ref_meta_data.samples > len(samples)
    ):
        raise TypeError(
            f"Cardinality of samples is less than requested size. Cannot"
            f"Fulfill request: size={ref_meta_data.samples} > |samples| ="
            f"{len(samples)}"
        )

    if isinstance(samples.data, FlatJson):
        raise NotImplementedError(
            "Sampling a Document Column is not yet supported"
        )
    elif isinstance(samples.data, list):
        num_nulls = rng.binomial(
            n=ref_meta_data.samples, p=ref_meta_data.null_rate
        )
        non_null_data = rng.choice(
            a=list(filter(lambda x: x is not None, samples.data)),
            replace=not ref_meta_data.unique,
            size=ref_meta_data.samples - num_nulls,
        )
        catted_vals = list(non_null_data) + [None for _ in range(num_nulls)]
        rng.shuffle(catted_vals)
        return catted_vals
    else:
        raise TypeError()


def traverse(
    rng: np.random.Generator, graph_gen: GraphGenerator
) -> tuple[dict[TCID, TCID], Collection[Column]]:
    """
    Traverses the graph, generating the random samples along the way until all
    the data has been sampled from the population.
    """
    black_cols: dict[TCID, Column] = dict()
    gray_cols: dict[TCID, RefMetaData] = dict()
    pointer_cols: dict[TCID, TCID] = dict()

    for table_gen in graph_gen.tables:
        for col_gen in table_gen.columns:
            tcid = TCID(table_gen.table_name, col_gen.column_name)

            if isinstance(col_gen.data, GeneratorData):
                # A ColBase is always generatable because it's a leaf node.

                # Add this col to the list of traversed cols
                black_cols[tcid] = Column(
                    tcid=tcid,
                    data=GENERATORS[col_gen.data.type](
                        rng,
                        table_gen.samples,
                        col_gen.data.unique,
                        col_gen.data.null_rate,
                        **col_gen.data.params,
                    ),
                )

            elif isinstance(col_gen.data, list):

                # Internal check because Python is dynamically typed (Sigh)
                for _inner in col_gen.data:
                    if not isinstance(_inner, JsonData):
                        raise TypeError(
                            f"Expected JsonData object, recieved "
                            f"{type(_inner)}\n{col_gen=}"
                        )

                black_cols[tcid] = Column(
                    tcid=tcid,
                    data=traverse_json(
                        rng,
                        RefMetaData(
                            table_gen.samples,
                            col_gen.unique,
                            col_gen.null_rate,
                        ),
                        col_gen.data,
                    ),
                )

            elif isinstance(col_gen.data, ReferenceData):
                # We will handle refs after generating all leaf nodes

                pointer_cols[tcid] = TCID(
                    col_gen.data.table, col_gen.data.column
                )
                gray_cols[tcid] = RefMetaData(
                    table_gen.samples,
                    col_gen.data.unique,
                    col_gen.data.null_rate,
                )

            else:
                # Oops
                raise TypeError(
                    f"Expected ColumnGenerator object, recieved {type(col_gen)}"
                    f"\n{col_gen=}"
                )

    second_gray_cols: set[TCID] = set()
    for col_id in gray_cols:

        # If the column was visited in a previous iteration it means it was
        # generated and so we can move onto the next column id
        if col_id in second_gray_cols:
            continue

        # Initialize "recursive" variables
        ref_id = pointer_cols[col_id]
        nested_refs: list[TCID] = [col_id]

        # Recursively follow references until you reach the base case
        while ref_id not in black_cols:

            # Hold onto the ref for generating later
            nested_refs.append(ref_id)

            # Follow to the next link
            ref_id = pointer_cols[ref_id]

        # Now pop the references off and generate them
        while len(nested_refs) > 0:
            gen_id = nested_refs.pop()
            ref_id = pointer_cols[gen_id]

            # Generate data
            full_samples = black_cols[ref_id]
            ref_meta_data = gray_cols[gen_id]
            _samples = subset_samples(rng, full_samples, ref_meta_data)

            # update variables
            black_cols[gen_id] = Column(
                tcid=gen_id, data=_samples, reference=ref_id
            )
            second_gray_cols.add(gen_id)

    return (pointer_cols, tuple(black_cols.values()))
