import itertools as itools
from typing import Collection

import numpy as np
import pandas as pd

from faker_rds.types import Column


def key_of_dict_iter(key_val: Column):
    return key_val.tcid.table_name


def export_to_pandas(
    columns: Collection[Column],
) -> list[tuple[str, pd.DataFrame]]:
    return [
        (
            key,
            pd.DataFrame(
                data={
                    k.tcid.column_name: k.data
                    if isinstance(k.data, np.ndarray)
                    else [x for x in k.data]
                    for k in grouping
                }
            ),
        )
        for key, grouping in itools.groupby(
            sorted(columns, key=key_of_dict_iter), key=key_of_dict_iter
        )
    ]
