import itertools as itools
import json
from typing import Collection

import numpy as np

from faker_rds.types import Column, Table, TCID


def _key_of_dict_iter(key_val: Column) -> str:
    return key_val.tcid.table_name


def group_tables_together(columns: Collection[Column]) -> Collection[Table]:
    return [
        Table(table_name=key, columns=tuple(grouping))
        for key, grouping in itools.groupby(
            sorted(columns, key=_key_of_dict_iter), key=_key_of_dict_iter
        )
    ]


def normalize_for_sql(x):
    if x == np.nan:
        # print(f"{x=} was nan")
        return None
    elif isinstance(x, np.integer):
        # print(f"{x=} was an int")
        return int(x)
    elif isinstance(x, np.floating):
        # print(f"{x=} was a float")
        return float(x)
    elif isinstance(x, np.str_):
        # print(f"{x=} was a str")
        return str(x)
    elif isinstance(x, np.datetime64):
        # print(f"{x=} was a dt")
        return str(x)
    elif isinstance(x, dict):
        # print(f"{x=} was a dict")
        return json.dumps(x)
    else:
        return x


def order_tables(
    tables: Collection[Table], col_refs: dict[TCID, TCID]
) -> Collection[Table]:
    """
    Given a collection of unordered tables, order the tables in the correct
    sequence required to be copied into the database.

    NOTE: This does not sort the rows of the tables or otherwise modifies the
    tables. Self-referential tables must be handled outside the scope of this
    function.
    """
    black_table_lookup: set[str] = set()

    black_tables: list[Table] = list()
    gray_tables: dict[str, tuple[Table, set[str]]] = dict()

    # Initial pass of the table collection. All independent tables are promoted
    # to the black table list while the others are kept in the gray table list
    for table in tables:
        table_deps: set[str] = set()
        for column in table.columns:
            tcid = column.reference
            while tcid is not None and tcid.table_name != table.table_name:
                table_deps.add(tcid.table_name)
                tcid = col_refs.get(tcid)
        if len(table_deps) == 0:
            black_tables.append(table)
            black_table_lookup.add(table.table_name)
        else:
            gray_tables[table.table_name] = (table, table_deps)

    # Run through the gray table list until all tables are promoted
    while len(gray_tables) > 0:

        # Cache the set of tables to be promoted at the end of iteration
        promoted_tables: set[str] = set()

        for (table, table_deps) in gray_tables.values():
            is_ready = True

            for dependency in table_deps:
                if dependency not in black_table_lookup:
                    is_ready = False

            if is_ready:
                black_table_lookup.add(table.table_name)
                black_tables.append(table)
                promoted_tables.add(table.table_name)

        # Delete the promoted tables
        for promoted_table in promoted_tables:
            del gray_tables[promoted_table]

    return black_tables
